from bottle import route, run, template,static_file
import sys,os
import fnmatch

CAPTURE_DIR="images"

@route('/')
def index():
    rootPath= os.path.dirname(sys.argv[0]) +'/'+ CAPTURE_DIR
    pattern = '*.jpg'
    imageList=[]
    for root, dirs, files in os.walk(rootPath):

        for filename in fnmatch.filter(files, pattern):

            x=os.path.split(root)
            if (x[1]!=CAPTURE_DIR):
                y=x[1]
            else:
                y=''
            imageList.append(os.path.join(y,filename))

    return template('hello_template', name=imageList)

@route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='./images/')

run(host='localhost', port=8080)