from lib import *
import pifacedigitalio

def toggle_led0(event):
    event.chip.leds[5].toggle()



#------------------------------------------------------------------------------
def main():
    """..."""
    evManager = EventManager()
    light= Lights(evManager)
    camera= Camera(evManager)
    keybd = KeyboardController(evManager)
    spinner = CPUSpinnerController(evManager)
    pygameView = PygameView(evManager)
    spinner.Run()


# pifacedigital = pifacedigitalio.PiFaceDigital()
# listener = pifacedigitalio.InputEventListener(chip=pifacedigital)
# listener.register(INPUT_BUTTON, pifacedigitalio.IODIR_FALLING_EDGE, toggle_led0)
# listener.activate()

if __name__ == '__main__':
    main()




