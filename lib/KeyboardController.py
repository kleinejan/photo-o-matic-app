from Event import *
from globals import *
import pygame
from pygame.locals import *
import pifacedigitalio as p

class KeyboardController:
    """..."""
    def __init__(self, evManager):
        self.evManager = evManager
        self.evManager.RegisterListener(self)
        p.init()

        self.bValuePrevious=0
        self.sValuePrevious=0

    def ButtonDown(self):
        if p.digital_read(INPUT_BUTTON):
            self.bValuePrevious=p.digital_read(INPUT_BUTTON)
        if p.digital_read(INPUT_BUTTON)==0 and self.bValuePrevious==1:
            self.bValuePrevious=0
            self.evManager.Post(BlinkLightEvent('START'))

    def SecretButtonDown(self):
        if p.digital_read(INPUT_SECRETBUTTON):
            self.sValuePrevious=p.digital_read(INPUT_SECRETBUTTON)
        if p.digital_read(INPUT_SECRETBUTTON)==0 and self.sValuePrevious==1:
            self.sValuePrevious=0
            self.evManager.Post(InstantPicture())

    #----------------------------------------------------------------------
    def Notify(self, event):
        if isinstance(event, TickEvent):
            # Handle input events.

            self.ButtonDown()
            self.SecretButtonDown()
            for event in pygame.event.get():
                ev = None
                if event.type == QUIT:
                    ev = QuitEvent()
                elif event.type == KEYDOWN and event.key == K_ESCAPE:
                    ev = QuitEvent()
                elif event.type == KEYDOWN and event.key == K_UP:
                    ev = QuitEvent()
                elif event.type == KEYDOWN and event.key == K_DOWN:
                    ev = InstantPicture()
                elif event.type == KEYDOWN and event.key == K_LEFT:
                    # ev = LightEvent('LED')
                    ev=BlinkLightEvent('START')
                elif event.type == KEYDOWN and event.key == K_RIGHT:
                    ev = DisplayPicture()
                if ev:
                    self.evManager.Post(ev)