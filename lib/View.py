from Event import *
import pygame
from PictureBox import *
from TimeLooper import *
from globals import *
class PygameView:
    """..."""
    def __init__(self, evManager):
        self.evManager = evManager
        self.evManager.RegisterListener(self)
        self.pic=PictureBox()
        self.run=False
        pygame.init()
        pygame.mouse.set_visible(False)
        self.window = pygame.display.set_mode((0,0),pygame.FULLSCREEN)
        pygame.display.set_caption('Photo-o-matic')
        self.background = pygame.Surface(self.window.get_size())
        self.background.fill((0, 0, 0))
        self.x=None;

#     #----------------------------------------------------------------------
    def ClearScreen(self):
        self.background = pygame.Surface(self.window.get_size())
        pygame.draw.rect(self.window, ( 0,   0,   0), [0, 0,self.background.get_width(), self.background.get_height()])


    def GetPicture(self):
        k=pygame.image.load(self.pic.GetRandom()).convert()
        picture=self.aspect_scale(k,[self.background.get_width(),self.background.get_height()])
        self.x=TimeLooper()

        return picture

    def GetLastPicture(self):

        k=pygame.image.load(self.pic.LastImage).convert()
        picture=self.aspect_scale(k,[self.background.get_width(),self.background.get_height()])
        self.x=TimeLooper(0,200,200)

        return picture

    def aspect_scale(self,img,(bx,by)):
        """ Scales 'img' to fit into box bx/by.
         This method will retain the original image's aspect ratio
         http://www.pygame.org/pcr/transform_scale/
         """
        ix,iy = img.get_size()
        if ix > iy:
            # fit to width
            scale_factor = bx/float(ix)
            sy = scale_factor * iy
            if sy > by:
                scale_factor = by/float(iy)
                sx = scale_factor * ix
                sy = by
            else:
                sx = bx
        else:
            # fit to height
            scale_factor = by/float(iy)
            sx = scale_factor * ix
            if sx > bx:
                scale_factor = bx/float(ix)
                sx = bx
                sy = scale_factor * iy
            else:
                sy = by

        return pygame.transform.scale(img, (int(sx),int(sy)))


    def DisplayPicture(self,picture):

        self.x.runner()
        # horizontal center the image
        pi = pygame.Surface(self.window.get_size())
        pi.fill((0, 0, 0))

        yOffset=(pi.get_height()-picture.get_height()) / 2
        self.DisplayStats()
        pi.blit(picture,(0,yOffset))
        pi.set_alpha(self.x.step)
        self.window.blit(pi,(0,0))

        pygame.display.flip()

        if self.x.t==False:
            self.evManager.Post(DisplayDone())


    def DisplayStats(self):

        TotalPictures=len(self.pic.imageList)+len(self.pic.imageListStash)
        # Fill background
        self.font = pygame.font.SysFont(None, 60)
        self.window.blit(self.font.render(str(TotalPictures), True, (100,100,100)), (0, 0))


    def Notify(self, event):
        if isinstance(event, TickEvent):
            pygame.display.update()
            if self.run:
                self.DisplayPicture(self.k)

        elif isinstance(event, NewImage):
            self.pic.ImageToList(event.filename)
            self.k=self.GetLastPicture()
            self.run=True
            self.DisplayPicture(self.k)

        elif isinstance(event,BlinkLightEvent):
            self.run=False
            self.ClearScreen()

        elif isinstance(event,CaptureEvent):
            self.run=False
            self.ClearScreen()

        elif isinstance(event, DisplayPicture):
            self.run=True
            self.k=self.GetPicture();

        elif isinstance(event, DisplayDone):
            self.run=True
            self.k=self.GetPicture();

