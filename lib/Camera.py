from Event import *
from globals import *
from datetime import datetime
import time
import picamera

class Camera:
    def __init__(self, evManager):
        self.evManager = evManager
        self.evManager.RegisterListener(self)

    def CaptureImage(self):



            filename=IMAGE_PREFIX + datetime.now().strftime('%Y-%m-%d-%H-%M%S')+'.jpg'
            outputFile=CAPTURE_DIR+'/'+filename

            print filename
            self.evManager.Post(LedEvent(LED_BUTTON,0))
            with picamera.PiCamera() as camera:
                # Camera warm-up time
                time.sleep(.2)
                # camera.hflip = False
                camera.vflip = True
                # camera.meter_mode='backlit'
                # camera.exposure_mode='sports'
                # camera.saturation=-50
                # camera.sharpness=10
                camera.contrast=20
                #
                # camera.framerate = 30
                # # Wait for analog gain to settle on a higher value than 1
                # while camera.analog_gain <= 1:
                #     time.sleep(0.1)
                # # Now fix the values
                # camera.shutter_speed = camera.exposure_speed
                # camera.exposure_mode = 'off'
                # g = camera.awb_gains
                # camera.awb_mode = 'off'
                # camera.awb_gains = g
                self.evManager.Post(LedEvent(LED_MAIN,1))
                self.evManager.Post(LedEvent(LED_SECODARY,1))
                camera.capture(outputFile,format='jpeg',quality=100)

            print "click"
            self.evManager.Post(NewImage(filename))
            self.evManager.Post(LedEvent(LED_MAIN,0))
            self.evManager.Post(LedEvent(LED_SECODARY,0))
            self.evManager.Post(LedEvent(LED_BUTTON,1))

    def Notify(self, event):
        if isinstance(event, CaptureEvent):
            self.evManager.Post(BlinkLightEvent('STOP'))
            self.CaptureImage()
        if isinstance(event, InstantPicture):
            self.evManager.Post(BlinkLightEvent('STOP'))
            self.CaptureImage()