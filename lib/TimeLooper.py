from tween import *
import pygame

class TimeLooper:
    def __init__(self,start=0,end=900,steps=50):
        self.run=False
        self.t = list(tween(easeLinear, start,end,steps,True,False))
        self.t2 = list(tween(easeInExpo, 0,255,10,True,False))
        self.run=True
        self.step=None

        self.waiter= pygame.time.get_ticks()+self.t.pop()

    def runner(self):
        if self.waiter <= pygame.time.get_ticks():
            pass
        else:
            try:
                self.waiter= self.t.pop() + self.waiter
                self.Popme()
            except IndexError:
                # Looper done
                self.t=False
                self.run=False

    def Popme(self):
        try:
            self.step=self.t2.pop(0)
        except IndexError:
            pass
