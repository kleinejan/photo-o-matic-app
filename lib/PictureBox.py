import os,sys,fnmatch,random
import imghdr
from globals import *

class PictureBox:

    def __init__(self):
        self.imageDir=CAPTURE_DIR
        self.imageList=self.loadimages()
        self.imageListStash=list()
        self.LastImage=None;

    def ImageToList(self,picture):
        # check if the image is a true picture
        image_type = imghdr.what(os.path.join(self.imageDir, picture))
        if image_type:
            #  if so we set the last
            self.LastImage= self.imageDir + '/' + picture
            self.imageList.append(self.LastImage)
        else:
            # else we return a random picture
            # should Fire a new capture event.
            self.LastImage= self.GetRandom()

    def loadimages(self):
        rootPath=self.imageDir
        pattern = '*.jpg'
        imageList=[]
        for root, dirs, files in os.walk(rootPath):
            for filename in fnmatch.filter(files, pattern):
                # check if the image is an image
                image_type = imghdr.what(os.path.join(root, filename))
                # while true we add it to the list
                if image_type:
                    imageList.append(os.path.join(root, filename))
        return imageList

    def GetRandom(self):
        # Get a random image from the list
        try:
            randompicture = random.choice(self.imageList)
        except IndexError:
            # We're out of pictures and copy the old ones back
            self.imageList=list(self.imageListStash)
            # empty Stash
            self.imageListStash=list()
            # And get a new random :)
            randompicture = random.choice(self.imageList)

        # Remove image from the big pool of image
        try:
            self.imageList.remove(randompicture)
        except ValueError:
            pass
        # Now we have used the image and stash it
        self.imageListStash.append(randompicture)
        print randompicture
        return randompicture
