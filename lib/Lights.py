from Event import *
from tween import *
from globals import *
import pygame

import pifacedigitalio


class Lights:
    def __init__(self, evManager):
        self.evManager = evManager
        self.evManager.RegisterListener(self)
        self.run=False
        self.led=False
        self.waiter=None
        self.t = list()
        self.pfd = pifacedigitalio.PiFaceDigital()

    def init(self):
        self.t = list(tween(easeInExpo, 100,1000,15,True,False))
        self.ToggleLight(LED_MAIN)
        self.waiter= pygame.time.get_ticks()+self.t.pop()
        self.run=True

    def LedControl(self,address,state):
        self.pfd.leds[address].value=state

    def Stop(self):
        self.run=False
        self.waiter=None
        self.LedControl(LED_MAIN,0);
        self.LedControl(LED_SECODARY,0);

    def Blink(self):
        if self.waiter >= pygame.time.get_ticks():
            pass
        else:
            # print int(time.time())
            self.led ^= True

            # self.window = pygame.display.set_mode((0,0),pygame.FULLSCREEN)
            if self.led:
                self.ToggleLight(LED_MAIN)
                self.ToggleLight(LED_SECODARY)
            else:
                self.ToggleLight(LED_MAIN)
                self.ToggleLight(LED_SECODARY)
            try:
                self.waiter=self.t.pop() + pygame.time.get_ticks()

            except IndexError:
                # self.run=False
                print 'index error'
                self.evManager.Post(CaptureEvent())
                self.Stop()


    def ToggleLight(self,address):
        self.pfd.leds[address].toggle()

    def Notify(self, event):
        if isinstance(event, BlinkLightEvent):
            if event.state=='START':
                self.Stop()
                self.init()
            if event.state=='STOP':
                self.Stop()
        if self.run:
            if isinstance(event, TickEvent):
                self.Blink()
        if isinstance(event, LedEvent):
            self.LedControl(event.number,event.state)